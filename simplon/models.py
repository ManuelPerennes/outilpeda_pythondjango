from django.db import models


class Formateur(models.Model):
         = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Student(models.Model):
    first_name_student = models.CharField(max_length=200)
    last_name_student = models.CharField(max_length=200)
    def __str__(self):
        return f"{self.first_name_student} {self.last_name_student}"


class Note(models.Model):
    author = models.ForeignKey(Formateur, on_delete=models.CASCADE)
    subjet = models.CharField(max_length=200)
    students = models.ManyToManyField(Student)
    notes = models.TextField(max_length=200)
    date = models.DateField("date published")

    def __str__(self):
        return f"De {self.author} : {self.subjet} {self.students}"
