from django.urls import path

from . import views

app_name = "simplon"
urlpatterns = [
    path("", views.accueil, name="accueil"),
    path("notes_by_student/", views.notes_by_student, name="notes_by_student"),
    path(
        "get_notes_by_student/<int:student_id>/",
        views.get_notes_by_student,
        name="get_notes_by_student",
    ),
]
