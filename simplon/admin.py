from django.contrib import admin

# Register your models here.
from django.contrib import admin

from .models import Formateur, Student, Note



admin.site.register(Formateur)
admin.site.register(Student)
admin.site.register(Note)
